
<font style="font-family:'Century Gothic', sans-serif; font-size: 10pt; color:#333;">
<table width="350" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td colspan="6"><font style="font-weight:bolder; color:#333333; font-size:11pt;">Jose Luis Lopez Lopez</font><br /><font style="color:#1F497D; font-size:10pt; font-weight:bold;">MSc Applied physics student at TU/e </font>
  <br />
 </td>
  </tr>
  <tr>
      <td width="40"><font style="color:#333333;font-weight:bold; font-style: italic; font-size:12px;">mobile: +31624121528</font></td>
  </tr>
</table></font>
<a href="https://www.linkedin.com/in/jlopezfisica" target="_blank"><img src="https://dl.dropboxusercontent.com/s/om95ji73xr4rlx2/linkedin.png?dl=0" width="32" height="32" alt="LinkedIn" /></a>
<a href="https://gitlab.com/u/jlopezfisicacode" target="_blank"><img src="https://dl.dropboxusercontent.com/s/ah0u2scmdm1w5m3/gitlab.png?dl=0" width="32" height="32" alt="LinkedIn" /></a>
<a href="https://www.tue.nl" target="_blank"><img src="https://dl.dropboxusercontent.com/s/drs12a1phvwq2ya/tue.svg?dl=0" width="128" height="32" alt="LinkedIn" /></a>
